import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevzaComponent } from './devza.component';

describe('DevzaComponent', () => {
  let component: DevzaComponent;
  let fixture: ComponentFixture<DevzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
