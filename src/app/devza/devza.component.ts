import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { NgxSpinnerService } from "ngx-spinner";

declare var $ :any;

@Component({
  selector: 'app-devza',
  templateUrl: './devza.component.html',
  styleUrls: ['./devza.component.css']
})
export class DevzaComponent implements OnInit {


  itemData :any =[];
  data:any=[];
  edit :boolean =false;

   constructor(public service: ApiService, private spinner: NgxSpinnerService) {
   }

   ngOnInit(): void {
    this.getTask();
    this.spinner.show();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 3000);
  }

  
   getTask() {
    this.service.getAllTask().then((Response) => {
     console.log(Response, "all data here available");
     
        if (Response.status == "success") {
            this.data = Response.tasks;
            console.log(this.data,"data");
            
        } else {
            console.log("Some thing went wrong");
            
        }
    },
    (Error) => {

    });
}

getDetails(item)
{
   this.edit = true;
   this.itemData.assigned_name = item.assigned_name;
   this.itemData.assigned_to = item.assigned_to;
   this.itemData.message = item.message;
   this.itemData.priority = item.priority;
   this.itemData.id = item.id;
   this.itemData.due_date = item.due_date;
   console.log("item",item);
   $('#updatetaskModal').modal('show');
}

openAddModel(){
  this.itemData=[];
  this.edit =false;
  $('#updatetaskModal').modal('show');
}
addTask(){

  this.edit = false;

  var formData = new FormData();
  formData.append('message',this.itemData.message);
  formData.append('due_date',this.itemData.due_date);
  formData.append('priority',this.itemData.priority);
  formData.append('assigned_to',this.itemData.assigned_to);
  this.service.addtask(formData).then((Response) => {
    console.log("resp",Response);
    
    if (Response.status == 'success') {
       this.getTask();
       $('#updatetaskModal').modal('hide');
      alert("Task Added Successfully");
      
    } else {
      
    }
  },
  (Error) => {

  });

}

updateDate(){
  
  var formData = new FormData();
  formData.append('message',this.itemData.message);
  formData.append('due_date',this.itemData.due_date);
  formData.append('priority',this.itemData.priority);
  formData.append('assigned_to',this.itemData.assigned_to);
  formData.append('taskid',this.itemData.assigned_to);

  this.service.updatedata(formData).then((Response) => {
    console.log("resp",Response);
    
    if (Response.status == 'success') {
      this.getTask();
      $('#updatetaskModal').modal('hide');
      alert("Task Updated Successfully");

    } else {
      
    }
  },
  (Error) => {

  });

}

selected_id_to_delete:any;
getDelete_id(item)
{
  this.selected_id_to_delete=item.id;
}

deleteData()
{
  var formData = new FormData();
  formData.append('taskid',this.selected_id_to_delete);
    this.service.deletetask(formData).then((Response) => {

    if (Response.status == 'success') {
      this.getTask();
        
      } else {
         
      }
    },
    (Error) => {

    });
  
}
}
