import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }



getAllTask(): Promise<any> {
  return new Promise<any>(resolve => {
    const headers = new HttpHeaders({
      'AuthToken': '3iVENG4Z7IMB5hwID9otftlo81QVA7It'
    });
    const httpOptions = {
      headers: headers
    };

    this.http.get('https://devza.com/tests/tasks/list' , httpOptions).subscribe(data => {
      resolve(data);

    },
      err => {
        // console.log(err);
        resolve(err);
      }
    );
  });
}


addtask( data = {}): Promise<any> {
  return new Promise<any>(resolve => {
      const headers = new HttpHeaders({
        'AuthToken': '3iVENG4Z7IMB5hwID9otftlo81QVA7It'
      });
    const httpOptions = {
      headers: headers
    };

    this.http.post('https://devza.com/tests/tasks/create',data, httpOptions).subscribe(data => {
      resolve(data);

    },
      err => {
        // console.log(err);
        resolve(err);
      }
    );
  });
}

updatedata( data = {}): Promise<any> {
  return new Promise<any>(resolve => {
      const headers = new HttpHeaders({
        'AuthToken': '3iVENG4Z7IMB5hwID9otftlo81QVA7It'
      });
    const httpOptions = {
      headers: headers
    };

    this.http.post('https://devza.com/tests/tasks/update',data, httpOptions).subscribe(data => {
      resolve(data);

    },
      err => {
        // console.log(err);
        resolve(err);
      }
    );
  });
}
deletetask( data = {}): Promise<any> {
  return new Promise<any>(resolve => {
      const headers = new HttpHeaders({
        'AuthToken': '3iVENG4Z7IMB5hwID9otftlo81QVA7It'
      });
    const httpOptions = {
      headers: headers
    };

    this.http.post('https://devza.com/tests/tasks/delete',data, httpOptions).subscribe(data => {
      resolve(data);

    },
      err => {
        // console.log(err);
        resolve(err);
      }
    );
  });
}
createTask(body){
  console.log(body,"all data here");
  
  const headers = new HttpHeaders({
    'AuthToken': '3iVENG4Z7IMB5hwID9otftlo81QVA7It'
  });
  const httpOptions = {
    headers: headers
  };
  return this.http.post('https://devza.com/tests/tasks/create' ,body,  httpOptions)
}
}
